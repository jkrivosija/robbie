# Robbie
**Facebook messenger chat bot**

Facebook chatbot with skill to responde to user with Chuck Norris joke or prank. Bot has been built with Botkit framework.

Facebook page: https://www.facebook.com/Robbie-488860201621098/

## Getting Started

- Get project to your local machine: git clone [this repo]
- Install dependencies: yarn install.
- For development use: yarn dev start.
- For development you need ngrok or some other tunneling tool.

.env is mandatory in this setup. For now it must have:
FACEBOOK_PAGE_ACCESS_TOKEN=(get this when create facebook app)
VERIFY_TOKEN= Your token

# Author
Jovica Krivosija

## License
This project is licensed under the MIT License.